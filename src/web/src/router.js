import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import PrivateRoute from './components/PrivateRoute'

import Auth from './screens/auth/Auth'
import Dashboard from './screens/dashboard/Dashboard'

const Root = () => (
    <Router>
        <Switch>
            <div>
                <Route exact path="/" component={Auth} />
                <PrivateRoute path="/app" component={Dashboard} />
            </div>
        </Switch>
    </Router>
)

export default Root;